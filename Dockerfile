FROM openjdk:8-jdk-alpine
MAINTAINER Kiryl Dzerabin
COPY /target/backend-SNAPSHOT.jar /bin/
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/bin/backend-SNAPSHOT.jar"]
EXPOSE 8900