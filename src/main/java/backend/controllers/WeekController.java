package backend.controllers;

import backend.dao.*;
import backend.model.AssignedPair;
import backend.model.ClassRoom;
import backend.model.Node;
import backend.validator.PairValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
public class WeekController {
    @Autowired
    private ListDAO listDAO;
    @Autowired
    private SubjectDAO subjectDAO;
    @Autowired
    private ClassRoomDAO classRoomDAO;
    @Autowired
    private TeacherDAO teacherDAO;
    @Autowired
    private TimetableDAO timetableDAO;
    @Autowired
    private WeekDAO weekDAO;

    public WeekController() {
        listDAO = new ListDAO();
        subjectDAO = new SubjectDAO();
        teacherDAO = new TeacherDAO();
        timetableDAO = new TimetableDAO();
        classRoomDAO = new ClassRoomDAO();
        weekDAO = new WeekDAO();
    }

    @GetMapping(value = "/init")
    public List getAll() {

        return listDAO.getAll();
    }

    @PostMapping(value = "/init/new")
    public void addNew(@RequestBody String newElement) {
        listDAO.addNew(newElement);
    }

    @PostMapping(value = "/init/delete")
    public void removeElement(@RequestBody String id) {
        listDAO.delete(UUID.fromString(id));
    }

    @PostMapping(value = "/init/edit")
    public void editElement(@RequestBody Node node) {
        listDAO.edit(node.getId(), node.getNode());
    }

    @GetMapping(value = "/geteverything")
    public List getEverything() {
        return timetableDAO.getAll();
    }

    @GetMapping(value = "/classrooms")
    public List getClassrooms() {
        return classRoomDAO.getAll();
    }

    @GetMapping(value = "/pairsbyclassrooms")
    public List getPairByClassrooms() {
        return timetableDAO.getTimetablebyClassroom();
    }


    @GetMapping(value = "/addnewweek")
    public void addNewWeek() {
        weekDAO.addNewWeek();
    }
    @GetMapping(value = "/getallallpairs")
    public List getAllAllPairs() {
       return timetableDAO.getAllAllTimetable();
    }
    @GetMapping(value = "/weeks")
    public List getWeeks() {
        return weekDAO.getAll();
    }
    @PostMapping(value = "/addclassroom")
    public String addClassRoom(@RequestBody ClassRoom classRoom){
        this.classRoomDAO.addNew(classRoom.getNumber(), classRoom.getPlaces());
        return "Success!";
    }
    @PostMapping(value = "/addnewPair")
    public String addPair(@RequestParam(name = "number") String number, @RequestParam(name = "number1") String number1){
        PairValidator pairValidator = new PairValidator();
        try{
            //pairValidator.addNewPair(assignedPair);
            System.out.println(number + " "+ number1);
            return "Success!";
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
}