package backend.dao;

import backend.model.ClassRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.util.List;
import java.util.UUID;

@Transactional
@Repository
public class ClassRoomDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;

    public ClassRoomDAO() {
        jdbcTemplate = new JdbcTemplate();
    }


    public List getAll() {
        String sql = "select * from public.classrooms;";
        RowMapper rowMapper = (rs, rowNum) -> {
            ClassRoom classRoom = new ClassRoom();
            classRoom.setId(UUID.fromString(rs.getString("id")));
            classRoom.setNumber(rs.getInt("classroom_number"));
            classRoom.setPlaces(rs.getInt("count_seats"));
            return classRoom;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    public ClassRoom getById(UUID uuid){
        String sql = "select * from public.classrooms WHERE id = '"+ uuid +"';";

        RowMapper rowMapper = (rs, rowNum) -> {
            ClassRoom classRoom = new ClassRoom();
            classRoom.setId(UUID.fromString(rs.getString("id")));
            classRoom.setNumber(rs.getInt("classroom_number"));
            classRoom.setPlaces(rs.getInt("count_seats"));
            return classRoom;
        };
        return (ClassRoom) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
    public void addNew(int newClassRoom, int countSeats) {
        String sql = "insert into " +
                "public.classrooms(id, classroom_number, count_seats) " +
                "values " +
                "('"+ UUID.randomUUID() +"', '"+newClassRoom+"','"+countSeats+"');";
        this.jdbcTemplate.update(sql);
    }


}
