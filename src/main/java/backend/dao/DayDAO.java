package backend.dao;

import backend.model.Day;
import backend.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;


@Transactional
@Repository
public class DayDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    public DayDAO() {
        jdbcTemplate = new JdbcTemplate();
    }
    public List getAll() {
        String sql = "select * from public.days;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Day day =new Day();
            day.setId(rs.getInt("id"));
            day.setDay(rs.getString("day"));
            return day;

        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Day getById(int id) {
        String sql = "select * from public.days WHERE id = " + id +";";
        RowMapper rowMapper = (rs, rowNum) -> {
            Day day =new Day();
            day.setId(rs.getInt("id"));
            day.setDay(rs.getString("day"));
            return day;
        };
        return (Day) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
}
