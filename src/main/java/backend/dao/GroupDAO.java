package backend.dao;

import backend.model.Day;
import backend.model.Group;
import backend.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;


@Transactional
@Repository
public class GroupDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    public GroupDAO() {
        jdbcTemplate = new JdbcTemplate();
    }
    public List getAll() {
        String sql = "select * from public.groups;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Group group =new Group();
            group.setId(rs.getInt("id"));
            group.setGroupNumber(rs.getInt("group_number"));
            group.setStudentCount(rs.getInt("students_count"));
            return group;

        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Group getById(int id) {
        String sql = "select * from public.groups WHERE id = "+ id +";";
        RowMapper rowMapper = (rs, rowNum) -> {
            Group group =new Group();
            group.setId(rs.getInt("id"));
            group.setGroupNumber(rs.getInt("group_number"));
            group.setStudentCount(rs.getInt("students_count"));
            return group;
        };
        return (Group) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
}
