package backend.dao;

import backend.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@Repository
public class ListDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    public ListDAO() {
        jdbcTemplate = new JdbcTemplate();
    }
    public List getAll() {
        String sql = "select * from public.list;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Node node =new Node();
            node.setNodeId(UUID.fromString(rs.getString("id")));
            node.setNode(rs.getString("node"));
            return node;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public void addNew(String newElement) {
        String sql = "insert into list(id, node) values ('"+ UUID.randomUUID() +"', '"+newElement+"');";
        this.jdbcTemplate.update(sql);
    }
    public void delete(UUID id) {
        String sql = "delete from list where id='"+id+"';";
        this.jdbcTemplate.update(sql);
    }
    public void edit(UUID id, String newValue) {
        String sql = "UPDATE list SET node = '"+newValue+"' WHERE id='"+id+"';";
        this.jdbcTemplate.update(sql);
    }
}