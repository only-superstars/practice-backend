package backend.dao;

import backend.model.Node;
import backend.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
@Transactional
@Repository
public class SubjectDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;


    public SubjectDAO() {
        jdbcTemplate = new JdbcTemplate();
    }


    public List getAll() {
        String sql = "select * from public.subjects;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Subject subject =new Subject();
            subject.setId(UUID.fromString(rs.getString("id")));
            subject.setName(rs.getString("subject_name")) ;
            return subject;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Subject getById(UUID id) {
        String sql = "select * from public.subjects WHERE id = '"+ id +"';";
        RowMapper rowMapper = (rs, rowNum) -> {
            Subject subject =new Subject();
            subject.setId(UUID.fromString(rs.getString("id")));
            subject.setName(rs.getString("subject_name")); ;
            return subject;
        };
        return (Subject) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
    public void addNew(String newElement) {
        String sql = "insert into public.subjects(id, subject_name) values ('"+ UUID.randomUUID() +"', '"+newElement+"');";
        this.jdbcTemplate.update(sql);
    }
    public void delete(UUID id) {
        String sql = "delete from public.subjects where id='"+id+"';";
        this.jdbcTemplate.update(sql);
    }
    public void edit(UUID id, String newValue) {
        String sql = "UPDATE public.subjects SET subject_name = '"+newValue+"' WHERE id='"+id+"';";
        this.jdbcTemplate.update(sql);
    }
}
