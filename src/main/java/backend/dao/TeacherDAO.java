package backend.dao;

import backend.model.Node;
import backend.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
@Transactional
@Repository
public class TeacherDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;


    public TeacherDAO() {
        jdbcTemplate = new JdbcTemplate();
    }


    public List getAll() {
        String sql = "select * from public.teachers;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Teacher teacher =new Teacher();
            teacher.setId(UUID.fromString(rs.getString("id")));
            teacher.setName(rs.getString("teacher_name")) ;
            return teacher;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Teacher getById(UUID id) {
        String sql = "select * from public.teachers WHERE id = '" + id +"';";
        RowMapper rowMapper = (rs, rowNum) -> {
            Teacher teacher =new Teacher();
            teacher.setId(UUID.fromString(rs.getString("id")));
            teacher.setName(rs.getString("teacher_name")) ;
            return teacher;
        };
        return (Teacher) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
    public void addNew(String newElement) {
        String sql = "insert into public.teachers(id, teacher_name) values ('"+ UUID.randomUUID() +"', '"+newElement+"');";
        this.jdbcTemplate.update(sql);
    }
}
