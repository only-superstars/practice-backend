package backend.dao;

import backend.model.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
@Repository
public class TimeDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    public TimeDAO() {
        jdbcTemplate = new JdbcTemplate();
    }
    public List getAll() {
        String sql = "select * from public.times  ORDER BY id ASC;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Time time =new Time();
            time.setId(rs.getInt("id"));
            time.setTime(rs.getString("time"));
            return time;

        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Time getById(int id) {
        String sql = "select * from public.times WHERE id = '"+ id +"';";
        RowMapper rowMapper = (rs, rowNum) -> {
            Time time =new Time();
            time.setId(rs.getInt("id"));
            time.setTime(rs.getString("time"));
            return time;

        };
        return (Time) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
}
