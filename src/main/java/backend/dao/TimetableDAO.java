package backend.dao;

import backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Transactional
@Repository
public class TimetableDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    private ClassRoomDAO classRoomDAO;
    @Autowired
    private DayDAO dayDAO;
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private TimeDAO timeDAO;
    @Autowired
    private TeacherDAO teacherDAO;
    @Autowired
    private SubjectDAO subjectDAO;
    @Autowired
    private WeekDAO weekDAO;

    public TimetableDAO() {
        jdbcTemplate = new JdbcTemplate();
        classRoomDAO = new ClassRoomDAO();
        dayDAO = new DayDAO();
        groupDAO = new GroupDAO();
        timeDAO = new TimeDAO();
        teacherDAO = new TeacherDAO();
        subjectDAO = new SubjectDAO();
        weekDAO = new WeekDAO();
    }


    public List getAll() {
        String sql = "select * " +
                "from public.assigned_pairs " +
                "order by id_week ASC, id_day ASC, id_classroom ASC, id_time ASC";
        RowMapper rowMapper = (rs, rowNum) -> {
            AssignedPair assignedPair = new AssignedPair();
            assignedPair.setId(UUID.fromString(rs.getString("id")));
            assignedPair.setClassRoom(classRoomDAO.getById(UUID.fromString(rs.getString("id_classroom"))));
            assignedPair.setDay(dayDAO.getById(rs.getInt("id_day")));
            assignedPair.setGroup(groupDAO.getById(rs.getInt("id_group")));
            assignedPair.setTime(timeDAO.getById(rs.getInt("id_time")));
            assignedPair.setTeacher(teacherDAO.getById(UUID.fromString(rs.getString("id_teacher"))));
            assignedPair.setSubject(subjectDAO.getById(UUID.fromString(rs.getString("id_subject"))));
            assignedPair.setWeekId(rs.getInt("id_week"));
            assignedPair.setWeekCount(rs.getInt("week_count"));
            return assignedPair;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    public List getWeekTimetable(int id) {
        String sql = "select * " +
                "from public.assigned_pairs " +
                "where id_week = '" + id + "'";
        RowMapper rowMapper = (rs, rowNum) -> {
            AssignedPair assignedPair = new AssignedPair();
            assignedPair.setId(UUID.fromString(rs.getString("id")));
            assignedPair.setClassRoom(classRoomDAO.getById(UUID.fromString(rs.getString("id_classroom"))));
            assignedPair.setDay(dayDAO.getById(rs.getInt("id_day")));
            assignedPair.setGroup(groupDAO.getById(rs.getInt("id_group")));
            assignedPair.setTime(timeDAO.getById(rs.getInt("id_time")));
            assignedPair.setTeacher(teacherDAO.getById(UUID.fromString(rs.getString("id_teacher"))));
            assignedPair.setSubject(subjectDAO.getById(UUID.fromString(rs.getString("id_subject"))));
            assignedPair.setWeekId(rs.getInt("id_week"));
            assignedPair.setWeekCount(rs.getInt("week_count"));
            return assignedPair;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    public List getTimetablebyClassroom() {
        List<AssignedPair> timetable = this.getAll();
        List<ClassRoom> classrooms = classRoomDAO.getAll();
        List<ClassRoomPair> classRoomPairs = new ArrayList();
        for (ClassRoom classroom : classrooms) {
            classRoomPairs.add(new ClassRoomPair(classroom.getNumber()));
        }
        for (AssignedPair pair : timetable) {
            for (ClassRoomPair classRoomPair : classRoomPairs)
                if (pair.getClassRoom().getNumber() == classRoomPair.getNumber()) {
                    classRoomPair.getPairs().add(pair);
                }
        }
        return classRoomPairs;
    }

    public void addNew(String newElement) {
        String sql = "insert into teachers(id, teacher_name) values ('" + UUID.randomUUID() + "', '" + newElement + "');";
        this.jdbcTemplate.update(sql);
    }

    public void delete(UUID id) {
        String sql = "delete from teachers where id='" + id + "';";
        this.jdbcTemplate.update(sql);
    }

    public void edit(UUID id, String newValue) {
        String sql = "UPDATE teachers SET teacher_name = '" + newValue + "' WHERE id='" + id + "';";
        this.jdbcTemplate.update(sql);
    }


    public List getAllAllTimetable() {
        List<WeekPairs> timeTable = new ArrayList<>();
        List<String> weekIds = weekDAO.getAllWeekIds();
        List<AssignedPair> allPairs = this.getAll();
        List<Time> times = timeDAO.getAll();
        List<Day> days = dayDAO.getAll();
        for (String week : weekIds) {
            List<TimePair> timePairsList = new ArrayList();
            for (Day day : days) {
                for (Time time : times) {
                    timePairsList.add(new TimePair(time.getTime(), day.getDay()));
                }
            }
            for (AssignedPair pair : allPairs) {
                for (int weekPair = 0; weekPair < pair.getWeekCount(); weekPair++) {
                    for (TimePair timePair : timePairsList)
                        if (pair.getTime().getTime().equals(timePair.getTime()) && pair.getDay().getDay().equals(timePair.getDay()) && Integer.toString(pair.getWeekId() + weekPair).equals(week)) {
                                timePair.getPairs().add(new AssignedTimePair(pair, pair.getWeekId() + weekPair, pair.getWeekCount() - weekPair));
                        }
                }
            }
            timeTable.add(new WeekPairs(week, timePairsList));
        }
        return timeTable;
    }
}
