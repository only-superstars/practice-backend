package backend.dao;

import backend.model.Time;
import backend.model.Week;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;


@Transactional
@Repository
public class WeekDAO {
    @Autowired
    private final JdbcTemplate jdbcTemplate;
    Calendar calendar;
    public WeekDAO() {
        jdbcTemplate = new JdbcTemplate();
        calendar = Calendar.getInstance();
    }
    public List getAll() {
        String sql = "select * from public.weeks;";
        RowMapper rowMapper = (rs, rowNum) -> {
            Week week =new Week();
            week.setId(rs.getInt("week_id"));
            week.setWeekStart(rs.getDate("week_start")) ;
            week.setWeekEnd(rs.getDate("week_finish"));
            return week;
        };
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Time getById(int id) {
        String sql = "select * from public.times WHERE id = '"+ id +"';";
        RowMapper rowMapper = (rs, rowNum) -> {
            Time time =new Time();
            time.setId(rs.getInt("id"));
            time.setTime(rs.getString("time"));
            return time;

        };
        return (Time) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
    public List getAllWeekIds(){
        String sql = "select week_id from public.weeks";
        RowMapper rowMapper = (rs, rowNum) -> rs.getString("week_id");
        return this.jdbcTemplate.query(sql, rowMapper);
    }
    public Week getLastWeek(){
        String sql = "select * from public.weeks where week_id = (select count(*) from public.weeks);";
        RowMapper rowMapper = (rs, rowNum) -> {
            Week week =new Week();
            week.setId(rs.getInt("week_id"));
            week.setWeekStart(rs.getDate("week_start"));
            week.setWeekEnd(rs.getDate("week_finish"));
            return week;
        };
        return (Week) this.jdbcTemplate.queryForObject(sql,rowMapper);
    }
    public void addNewWeek() {
        Week lastWeek = this.getLastWeek();
        calendar.setTime(lastWeek.getWeekStart());
        calendar.add(Calendar.DATE, 7);
        Date newWeekStart = new Date(calendar.getTimeInMillis());
        calendar.setTime(lastWeek.getWeekEnd());
        calendar.add(Calendar.DATE, 7);
        Date newWeekEnd = new Date(calendar.getTimeInMillis());
        String sql = "insert into public.weeks(week_id, week_start , week_finish) values ('"+ (lastWeek.getId()+1)  +"', '"+newWeekStart+"', '"+newWeekEnd +"');";
        this.jdbcTemplate.update(sql);
    }
}
