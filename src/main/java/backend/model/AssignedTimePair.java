package backend.model;

import java.util.UUID;

public class AssignedTimePair {
    private UUID id;
    private Subject subject;
    private Teacher teacher;
    private ClassRoom classRoom;
    private Day day;
    private Time time;
    private Group group;
    private int endsIn;


    private int weekId;

    public int getEndsIn() {
        return endsIn;
    }

    public void setEndsIn(int endsIn) {
        this.endsIn = endsIn;
    }

    public AssignedTimePair(AssignedPair assignedPair, int weekId, int endsIn  ){
        this.id = assignedPair.getId();
        this.subject = assignedPair.getSubject();
        this.teacher = assignedPair.getTeacher();
        this.classRoom = assignedPair.getClassRoom();
        this.day = assignedPair.getDay();
        this.time = assignedPair.getTime();
        this.group = assignedPair.getGroup();
        this.weekId = weekId;
        this.endsIn = endsIn;
    }
    public int getWeekId() {
        return weekId;
    }

    public void setWeekId(int weekId) {
        this.weekId = weekId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

}
