package backend.model;

import java.util.ArrayList;
import java.util.List;

public class ClassRoomPair {
    private int number;
    private List<AssignedPair> pairs;

    public ClassRoomPair(int number){
        this.number=number;
        pairs = new ArrayList<>();
    }
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<AssignedPair> getPairs() {
        return pairs;
    }

    public void setPairs(List<AssignedPair> pairs) {
        this.pairs = pairs;
    }
}
