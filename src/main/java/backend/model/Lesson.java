package backend.model;

import java.sql.Time;
import java.util.UUID;

public class Lesson {
    private UUID id;
    private Time time;
    private AssignedPair assignedPair;
    private ClassRoom classRoom;

    public Lesson(UUID id, Time time, AssignedPair assignedPair, ClassRoom classRoom) {
        this.id = id;
        this.time = time;
        this.assignedPair = assignedPair;
        this.classRoom = classRoom;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public AssignedPair getAssignedPair() {
        return assignedPair;
    }

    public void setAssignedPair(AssignedPair assignedPair) {
        this.assignedPair = assignedPair;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }
}
