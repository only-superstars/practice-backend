package backend.model;

import java.util.UUID;

public class Node {
    private UUID id;
    private String node;

    public UUID getId() {
        return id;
    }

    public void setNodeId(UUID id) {
        this.id = id;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
}
