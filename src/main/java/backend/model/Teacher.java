package backend.model;

import java.util.List;
import java.util.UUID;

public class Teacher {
    private UUID id;
    private String name;
    //private List<Subject> subjectList;
    //private List<Lesson> lessonList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //public List<Subject> getSubjectList() {return subjectList;}

    //public void setSubjectList(List<Subject> subjectList) {this.subjectList = subjectList;}

    //public List<Lesson> getLessonList() {return lessonList;}

    //public void setLessonList(List<Lesson> lessonList) {this.lessonList = lessonList;}
}
