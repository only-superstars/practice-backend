package backend.model;

import java.util.ArrayList;
import java.util.List;

public class TimePair {
    private String time;
    private String day;
    private List<AssignedTimePair> pairs;

    public TimePair(String time, String day) {
        this.time = time;
        this.day = day;
        pairs = new ArrayList<>();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<AssignedTimePair> getPairs() {
        return pairs;
    }

    public void setPairs(List<AssignedTimePair> pairs) {
        this.pairs = pairs;
    }
}
