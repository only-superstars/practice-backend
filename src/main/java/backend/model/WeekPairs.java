package backend.model;

import java.util.List;
import java.util.UUID;

public class WeekPairs {
    private String id;
    private List<TimePair> weekPairs;

    public String getId() {
        return id;
    }

    public WeekPairs(String id, List<TimePair> weekPairs) {
        this.id = id;
        this.weekPairs = weekPairs;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TimePair> getWeekPairs() {
        return weekPairs;
    }

    public void setWeekPairs(List<TimePair> weekPairs) {
        this.weekPairs = weekPairs;
    }
}
